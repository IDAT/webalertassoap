
package pe.edu.idat.sos.bean;

import java.util.Date;
import pe.edu.idat.sos.model.AlertaModel;

public class AlertaBean extends AlertaModel{
    String ale_estdes;

    public AlertaBean() {
    }

    public AlertaBean(String ale_correl, Date ale_fecreg, String ale_tipo, String ale_titulo, String ale_observ, int ale_estcod, String ale_estdes) {
        super(ale_correl, ale_fecreg, ale_tipo, ale_titulo, ale_observ, ale_estcod);
        this.ale_estdes = ale_estdes;
    }

    public String getAle_estdes() {
        return ale_estdes;
    }

    public void setAle_estdes(String ale_estdes) {
        this.ale_estdes = ale_estdes;
    }
    
}