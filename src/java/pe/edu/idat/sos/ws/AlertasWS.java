package pe.edu.idat.sos.ws;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import pe.edu.idat.sos.bean.AlertaBean;
import pe.edu.idat.sos.dao.AlertaDAOImpl;

/**
 *
 * @author alex
 */
@WebService(serviceName = "AlertasWS")
public class AlertasWS {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "SelAlertaAll")
    public String SelAlertaAll(@WebParam(name = "arg_anio") String arg_anio, @WebParam(name = "arg_estcod") int arg_estcod) {
        //TODO - JSON RESULT
        JsonObject jsonResult = new JsonObject();
        ArrayList<AlertaBean> lst = null;
        try {
            AlertaDAOImpl AlertaDAO = new AlertaDAOImpl();
            lst = AlertaDAO.SelAlertaAll(arg_anio, arg_estcod);
            
            JsonElement jElement = new Gson().toJsonTree(lst);
            jsonResult.addProperty("status", Boolean.TRUE);
            jsonResult.addProperty("message", lst.size() + " REGISTRO(S) ENCONTRADO(S)");
            jsonResult.add("data", jElement);
        } catch (Exception e) {
            jsonResult.addProperty("status", Boolean.FALSE);
            jsonResult.addProperty("message", e.getMessage());
        }
        return jsonResult.toString();
    }
     @WebMethod(operationName = "InsAlerta")
    public String InsAlerta(@WebParam(name = "arg_tipo") String arg_tipo, @WebParam(name = "arg_titulo") String arg_titulo,
            @WebParam(name = "arg_observ") String arg_observ, @WebParam(name = "arg_estcod") int arg_estcod) {
        //TODO - JSON RESULT
        JsonObject jsonResult = new JsonObject();
        try {
            AlertaDAOImpl AlertaDAO = new AlertaDAOImpl();
            jsonResult.addProperty("status", Boolean.TRUE);
            AlertaBean alertaBean = new AlertaBean();
            alertaBean.setAle_tipo(arg_tipo);
            alertaBean.setAle_titulo(arg_titulo);
            alertaBean.setAle_observ(arg_observ);
            alertaBean.setAle_estcod(arg_estcod);
            boolean bResult = AlertaDAO.InsAlerta(alertaBean);
            jsonResult.addProperty("message","{0} REGISTRO INSERTADO".replace("{0}", (bResult ? "1" :"0")));
        } catch (Exception e) {
            jsonResult.addProperty("status", Boolean.FALSE);
            jsonResult.addProperty("message", e.getMessage());
        }
        return jsonResult.toString();
    }
    @WebMethod(operationName = "UpdAlerta")
    public String UpdAlerta(@WebParam(name = "arg_tipo") String arg_tipo, @WebParam(name = "arg_titulo") String arg_titulo,
            @WebParam(name = "arg_observ") String arg_observ, @WebParam(name = "arg_estcod") int arg_estcod,
            @WebParam(name = "arg_correl") String arg_correl) {
        //TODO - JSON RESULT
        JsonObject jsonResult = new JsonObject();
        try {
            AlertaDAOImpl AlertaDAO = new AlertaDAOImpl();
            jsonResult.addProperty("status", Boolean.TRUE);
            AlertaBean alertaBean = new AlertaBean();
            alertaBean.setAle_correl(arg_correl);
            alertaBean.setAle_tipo(arg_tipo);
            alertaBean.setAle_titulo(arg_titulo);
            alertaBean.setAle_observ(arg_observ);
            alertaBean.setAle_estcod(arg_estcod);
            boolean bResult = AlertaDAO.UpdAlerta(alertaBean);
            jsonResult.addProperty("message","{0} REGISTRO ACTUALIZADO".replace("{0}", (bResult ? "1" :"0")));
        } catch (Exception e) {
            jsonResult.addProperty("status", Boolean.FALSE);
            jsonResult.addProperty("message", e.getMessage());
        }
        return jsonResult.toString();
    }
    
    @WebMethod(operationName = "DelAlertaAll")
    public String DelAlerta(@WebParam(name = "arg_correl") String arg_correl) {
        //TODO - JSON RESULT
        JsonObject jsonResult = new JsonObject();
        try {
            AlertaDAOImpl AlertaDAO = new AlertaDAOImpl();
            jsonResult.addProperty("status", Boolean.TRUE);
            boolean bResult = AlertaDAO.DelAlerta(arg_correl);
            jsonResult.addProperty("message","{0} REGISTRO ELIMINADO".replace("{0}", (bResult ? "1" :"0")));
        } catch (Exception e) {
            jsonResult.addProperty("status", Boolean.FALSE);
            jsonResult.addProperty("message", e.getMessage());
        }
        return jsonResult.toString();
    }

  
}
