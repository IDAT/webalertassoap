
package pe.edu.idat.sos.model;

import java.util.Date;

public abstract class AlertaModel {
    String ale_correl;
    Date ale_fecreg;
    String ale_tipo;
    String ale_titulo;
    String ale_observ;
    int ale_estcod;

    public AlertaModel() {
    }

    public AlertaModel(String ale_correl, Date ale_fecreg, String ale_tipo, String ale_titulo, String ale_observ, int ale_estcod) {
        this.ale_correl = ale_correl;
        this.ale_fecreg = ale_fecreg;
        this.ale_tipo = ale_tipo;
        this.ale_titulo = ale_titulo;
        this.ale_observ = ale_observ;
        this.ale_estcod = ale_estcod;
    }

    public String getAle_correl() {
        return ale_correl;
    }

    public void setAle_correl(String ale_correl) {
        this.ale_correl = ale_correl;
    }

    public Date getAle_fecreg() {
        return ale_fecreg;
    }

    public void setAle_fecreg(Date ale_fecreg) {
        this.ale_fecreg = ale_fecreg;
    }

    public String getAle_tipo() {
        return ale_tipo;
    }

    public void setAle_tipo(String ale_tipo) {
        this.ale_tipo = ale_tipo;
    }

    public String getAle_titulo() {
        return ale_titulo;
    }

    public void setAle_titulo(String ale_titulo) {
        this.ale_titulo = ale_titulo;
    }

    public String getAle_observ() {
        return ale_observ;
    }

    public void setAle_observ(String ale_observ) {
        this.ale_observ = ale_observ;
    }

    public int getAle_estcod() {
        return ale_estcod;
    }

    public void setAle_estcod(int ale_estcod) {
        this.ale_estcod = ale_estcod;
    }
    
    
}
