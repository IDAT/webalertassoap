
package pe.edu.idat.sos.dao;

import java.util.ArrayList;
import pe.edu.idat.sos.bean.AlertaBean;

public interface AlertaDAO {
    public ArrayList<AlertaBean> SelAlertaAll(String anio, int estado)throws Exception;
    public boolean InsAlerta(AlertaBean alertaBean)throws Exception;
    public boolean UpdAlerta(AlertaBean alertaBean)throws Exception;
    public boolean DelAlerta(String correl)throws Exception;
}
