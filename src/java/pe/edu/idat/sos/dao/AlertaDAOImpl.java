/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.idat.sos.dao;

import pe.edu.idat.sos.util.ConexionMySQL;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import pe.edu.idat.sos.bean.AlertaBean;

public class AlertaDAOImpl extends ConexionMySQL implements AlertaDAO {
    
    public static void main(String[] args){
        
        try {
            AlertaDAOImpl AlertaDAO = new AlertaDAOImpl();
            AlertaBean alertaBean = new AlertaBean();
            alertaBean.setAle_tipo("A");
            alertaBean.setAle_titulo("SISTEMA ELECTRICO");
            alertaBean.setAle_tipo("MAQUINA EN MAL ESTADO");
            AlertaDAO.InsAlerta(alertaBean);
            
            ArrayList<AlertaBean> lst = AlertaDAO.SelAlertaAll("2018", 1);
            for(AlertaBean item: lst){
                System.out.println(item.getAle_titulo());
            }
        } catch (Exception ex) {
            Logger.getLogger(AlertaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }

    @Override
    public ArrayList<AlertaBean> SelAlertaAll(String anio, int estado) throws Exception {
        ArrayList<AlertaBean> lista = null;
        
        CallableStatement cstm = null;
        ResultSet res = null;
        try {
            cstm = getConexion().prepareCall("{call prcAlertaSelect(?,?)}");
            cstm.setString(1, anio);
            cstm.setInt(2, estado);
            cstm.execute();
            res = cstm.getResultSet();
            lista = new ArrayList<>();
            while(res.next()){
                AlertaBean b = new AlertaBean();
                b.setAle_correl(res.getString("ale_correl"));
                b.setAle_fecreg(res.getDate("ale_fecreg"));
                b.setAle_tipo(res.getString("ale_tipo"));
                b.setAle_titulo(res.getString("ale_titulo"));
                b.setAle_observ(res.getString("ale_observ"));
                b.setAle_estcod(res.getInt("ale_estcod"));
              
               lista.add(b);               
            }            
        } catch (SQLException e) {
            Logger.getLogger(AlertaDAOImpl.class.getName()).log(Level.SEVERE, null, e);
        }        
        return lista;
    
    }

    @Override
    public boolean InsAlerta(AlertaBean alertaBean) throws Exception {
        CallableStatement cstm = null;
        int res = 0;
        try {
            cstm = getConexion().prepareCall("{call prcAlertaInsert(?,?,?,?,?)}");
            cstm.setString(1, alertaBean.getAle_tipo());
            cstm.setString(2, alertaBean.getAle_titulo());
            cstm.setString(3, alertaBean.getAle_observ());
            cstm.setInt(4, alertaBean.getAle_estcod());
            res = cstm.executeUpdate();
        } catch (SQLException e) {
            Logger.getLogger(AlertaDAOImpl.class.getName()).log(Level.SEVERE, null, e);
        }        
        return res == 1;
    }

    @Override
    public boolean UpdAlerta(AlertaBean alertaBean) throws Exception {
        CallableStatement cstm = null;
        int res = 0;
        try {
            cstm = getConexion().prepareCall("{call prcAlertaUpdate(?,?,?,?,?)}");
            cstm.setString(1, alertaBean.getAle_tipo());
            cstm.setString(2, alertaBean.getAle_titulo());
            cstm.setString(3, alertaBean.getAle_observ());
            cstm.setInt(4, alertaBean.getAle_estcod());
            cstm.setString(5, alertaBean.getAle_correl());
            res = cstm.executeUpdate();
        } catch (SQLException e) {
            Logger.getLogger(AlertaDAOImpl.class.getName()).log(Level.SEVERE, null, e);
        }        
        return res == 1; 
    
    }

    @Override
    public boolean DelAlerta(String correl) throws Exception {
        CallableStatement cstm = null;
        int res = 0;
        try {
            cstm = getConexion().prepareCall("{call prcAlertaDelete(?)}");
            cstm.setString(1, correl);
            res = cstm.executeUpdate();
            
        } catch (SQLException e) {
            Logger.getLogger(AlertaDAOImpl.class.getName()).log(Level.SEVERE, null, e);
        }        
        return res == 1;       
    
    }

}