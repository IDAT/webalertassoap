-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 28-06-2018 a las 01:16:07
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbterminal`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcAlertaDelete` (IN `in_correl` CHAR(14))  BEGIN
   
    DELETE FROM termovalerta WHERE ale_correl = in_correl ;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcAlertaInsert` (IN `in_tipo` CHAR(1), IN `in_titulo` VARCHAR(100), `in_observ` VARCHAR(200), `in_estcod` INT, OUT `out_correl` CHAR(14))  BEGIN
	-- DECLARE CORREL CHAR(14);

	SET out_correl = (SELECT 
				CONCAT('ALE-',YEAR(NOW()),SUBSTRING(CONCAT('00000',SUBSTRING(IFNULL(MAX(ale_correl), 0),-6) + 1),-6))
				FROM termovalerta 
				WHERE SUBSTRING(ale_correl,5,4) = YEAR(NOW()));
   
    INSERT INTO termovalerta
	(ale_correl, ale_fecreg, ale_tipo, ale_titulo, ale_observ, ale_estcod)
	VALUES(out_correl, NOW(), in_tipo, in_titulo, in_observ, in_estcod);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcAlertaSelect` (IN `in_anio` CHAR(4), IN `in_estcod` INT)  BEGIN
   
    SELECT * FROM termovalerta WHERE YEAR(ale_fecreg)=in_anio AND ale_estcod = in_estcod ;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `termovalerta`
--

CREATE TABLE `termovalerta` (
  `ale_correl` char(14) NOT NULL,
  `ale_fecreg` datetime DEFAULT NULL,
  `ale_tipo` char(1) DEFAULT NULL,
  `ale_titulo` varchar(100) DEFAULT NULL,
  `ale_observ` varchar(200) DEFAULT NULL,
  `ale_estcod` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `termovalerta`
--

INSERT INTO `termovalerta` (`ale_correl`, `ale_fecreg`, `ale_tipo`, `ale_titulo`, `ale_observ`, `ale_estcod`) VALUES
('ALE-2018000001', '2018-06-21 00:00:00', 'M', 'MAQUINA CORTADORA', 'PERDIDA DE ENERGIA', 1),
('ALE-2018000002', '2018-06-20 00:00:00', 'D', 'MAQUINA NO ENCIENDE', 'SISTEMA ELECTRICO SIN ENERGIA', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `termovmarcaciones`
--

CREATE TABLE `termovmarcaciones` (
  `mar_correl` char(14) NOT NULL,
  `mar_fecreg` datetime DEFAULT NULL,
  `mar_percod` char(8) DEFAULT NULL,
  `mar_gpslat` double DEFAULT NULL,
  `mar_gpslng` double DEFAULT NULL,
  `mar_modelo` varchar(50) DEFAULT NULL,
  `mar_nivbat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `termovmarcaciones`
--

INSERT INTO `termovmarcaciones` (`mar_correl`, `mar_fecreg`, `mar_percod`, `mar_gpslat`, `mar_gpslng`, `mar_modelo`, `mar_nivbat`) VALUES
('MAR-2017000001', '2017-06-21 00:00:00', '42455979', 750000, 3200000, 'SAMSUNG', 89);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `termovalerta`
--
ALTER TABLE `termovalerta`
  ADD PRIMARY KEY (`ale_correl`);

--
-- Indices de la tabla `termovmarcaciones`
--
ALTER TABLE `termovmarcaciones`
  ADD PRIMARY KEY (`mar_correl`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
