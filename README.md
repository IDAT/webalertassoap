## Proyecto WebAlertaSoap

Basado en Java EE Servicio SOAP basado en javax.jws con conexión a base de datos MySQL:


1. Importar `dbterminal.sql` Ubicado en la carpeta `Web Pages/database`.
2. Aperture el proyecto con NetBeans y realice las pruebas con servidor de aplicaciones Glassfish o Tomcat.

Desarrollado por : [superahacker]

## @Controller (AlertaWS.java)

Servicio SOAP preparado para trabajar metodos `POST`

### Url WSDL
`http://localhost:8080/WebAlertas/AlertasWS?WSDL`

### Url Tester
`http://localhost:8080/WebAlertas/AlertasWS?Tester`

[superahacker]: http://superahacker.blogspot.com/